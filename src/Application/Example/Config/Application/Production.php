<?php
namespace Example\Config\Application {

    use AtomPie\Boundary\System\IAmEnvVariable;
    use AtomPie\Core\ApplicationConfig;

    class Production extends ApplicationConfig
    {
        const ACCEPT_API_VERSION_HEADER = 'acceptApiVersionHeader';

        /**
         * Fill config with data in constructor.
         * @param IAmEnvVariable $oEnv
         */
        public function __construct(IAmEnvVariable $oEnv)
        {
            $this->set(self::ACCEPT_API_VERSION_HEADER,'application/vnd.atompie+json');
            $this->set('mysqlHost','127.0.0.1');
            $this->set('mysqlDatabase','test');
            $this->set('mysqlUser','root');
            $this->set('mysqlPassword','root');
        }
        
    }

}
