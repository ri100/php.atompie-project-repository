<?php
namespace Example\EndPoint {

    use AtomPie\AnnotationTag\EndPoint;

    class UserRepository
    {
        /**
         * @EndPoint(ContentType="application/json")
         * @param \Example\Repository\MySql\UserRepository $repo
         * @return array
         */
        public function __default(\Example\Repository\MySql\UserRepository $repo) {
            return $repo->load();
        }
    }

}
