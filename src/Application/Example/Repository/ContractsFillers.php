<?php
namespace Example\Repository {

    use AtomPie\Boundary\Core\IAmContractDependency;
    use Example\Repository\Boundary\UserRepositoryInterface;
    use Example\Repository\MySql\UserRepository;

    class ContractsFillers implements IAmContractDependency
    {
        /**
         * Returns array of services that will fill
         * the contract/interface in form of
         *
         * [
         *     Interface::class => Service::class
         * ]
         *
         * @return array
         */
        public function provideContractDependencies()
        {
            return [
                UserRepositoryInterface::class => UserRepository::class
            ];
        }
    }

}
