<?php
namespace Example\Repository\MySql {

    use AtomPie\Boundary\Core\IAmApplicationConfig;
    use Example\Repository\Boundary\UserRepositoryInterface;
    use Example\Repository\Infrastructure\MySql\Database;

    class UserRepository implements UserRepositoryInterface
    {
        /**
         * @var \Example\Repository\Boundary\DatabaseInterface
         */
        private $oDatabase;

        public function __construct($sHost, $sDatabase, $sUser, $sPassword)
        {
            $oDatabase = new Database(
                $sHost,
                $sDatabase,
                $sUser,
                $sPassword
            );
            
            $this->oDatabase = $oDatabase;
        }

        /**
         * @return array
         */
        public function load()
        {
            return $this->oDatabase->query();
        }

        /////////////////////////////
        // Injection

        static function __build(IAmApplicationConfig $oConfig)
        {
            return new UserRepository(
                $oConfig->mysqlHost,
                $oConfig->mysqlDatabase,
                $oConfig->mysqlUser,
                $oConfig->mysqlPassword);
        }

        static function __constrainBuild() {
            return [
                \Example\EndPoint\UserRepository::class
            ];
        }
    }

}
