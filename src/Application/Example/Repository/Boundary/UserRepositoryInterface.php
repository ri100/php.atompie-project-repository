<?php
namespace Example\Repository\Boundary;

interface UserRepositoryInterface
{
    public function load();
}