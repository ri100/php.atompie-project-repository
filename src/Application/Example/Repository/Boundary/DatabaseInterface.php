<?php
namespace Example\Repository\Boundary;

interface DatabaseInterface
{
    public function query();
}