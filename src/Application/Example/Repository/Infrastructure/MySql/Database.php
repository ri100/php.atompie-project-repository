<?php
namespace Example\Repository\Infrastructure\MySql;

use Example\Repository\Boundary\DatabaseInterface;

class Database implements DatabaseInterface
{
    public function __construct($sHost, $sDatabase, $sUser, $sPassword)
    {
        // Connect to db.
    }
    
    public function query() {
        return [
            'id' => 1,
            'name' => 'test'
        ];
    }
}
