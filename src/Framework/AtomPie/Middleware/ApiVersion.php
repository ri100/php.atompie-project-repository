<?php
namespace AtomPie\Middleware {

    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\Boundary\System\IRunBeforeMiddleware;
    use AtomPie\System\Exception;
	use AtomPie\Web\Boundary\IChangeRequest;
	use AtomPie\Web\Boundary\IChangeResponse;
	use AtomPie\Web\Connection\Http\Header;

    class ApiVersion implements IRunBeforeMiddleware {

        /**
         * @var string
         */
		private $sVersionedApiNamespace;
        /**
         * @var IAmFrameworkConfig
         */
        private $oFrameworkConfig;

        public function __construct($sVersionedApiNamespace, IAmFrameworkConfig $oFrameworkConfig) {
			$this->sVersionedApiNamespace = $sVersionedApiNamespace;
            $this->oFrameworkConfig = $oFrameworkConfig;
        }

		/**
		 * Returns modified Request.
		 *
		 * @param IChangeRequest $oRequest
		 * @param IChangeResponse $oResponse
		 * @return IChangeRequest
		 * @throws Exception
		 */
		public function before(IChangeRequest $oRequest, IChangeResponse $oResponse) {
			if($oRequest->hasHeader(Header::ACCEPT)) {
				/** @var Header\Accept $oAccept */
				$oAccept = $oRequest->getHeader(Header::ACCEPT);

                $sVersionHeader = 'application/vnd.atompie+json';

                if($oAccept->willYouAcceptMediaType($sVersionHeader, true)) {
					$oMediaType = $oAccept->getMediaType($sVersionHeader);
					if(isset($oMediaType->params['version'])) {

                        $sCurrentVersionNamespace = $this->getVersionNamespace($oMediaType->params['version']);

                        if(!$this->oFrameworkConfig->hasEndPointNamespace($sCurrentVersionNamespace)) {
                            $this->oFrameworkConfig->prependEndPointNamespace($sCurrentVersionNamespace);
                        }
                        
					}
				}
			}
			return $oRequest;
		}

        /**
         * @param $sVersion
         * @return string
         */
        private function getVersionNamespace($sVersion)
        {
            return
                $this->sVersionedApiNamespace .
                '\\v' . str_replace('.', '_', $sVersion);
        }

    }

}
