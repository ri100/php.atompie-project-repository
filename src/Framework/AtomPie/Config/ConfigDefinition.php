<?php
namespace AtomPie\Config;

use AtomPie\Boundary\Core\IAmApplicationConfigDefinition;
use Example\Config\Application\DevConfig;
use Example\Config\Application\Production;

class ConfigDefinition implements IAmApplicationConfigDefinition
{

    /**
     * Returns default config class type. One that is used
     * if no class is returned by getLocalConfig() method.
     *
     * @return string
     */
    public function defaultConfig()
    {
        return Production::class;
    }

    /**
     * Returns local config class to be used depending on
     * environment used. In order to change how the environment is
     * determined override provide method.
     *
     * @return string
     */
    public function getLocalConfig()
    {
        return DevConfig::class;
    }
}
