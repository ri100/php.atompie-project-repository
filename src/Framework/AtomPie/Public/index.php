<?php
namespace {

    $oLoader = require_once __DIR__ . '/../../../../vendor/autoload.php';

    use AtomPie\Config\ConfigDefinition;
    use AtomPie\Core\Config;
    use AtomPie\Core\FrameworkConfig;
    use AtomPie\System\Kernel;
    use AtomPie\System\Router;
    use AtomPie\Web\Environment;
    use AtomPie\Middleware\ApiVersion;

    ini_set('display_errors', '1');
    ini_set("log_errors", 1);

    $oEnvironment = Environment::getInstance();
    $oConfig = new FrameworkConfig(
        $oEnvironment,
        new Router(__DIR__ . '/../Routing/Router.php'),
        new ConfigDefinition(),
        __DIR__,
        __DIR__ . '/../View',
        [
            'Example\EndPoint', // default namespace for endpoints
        ],
        [],
        [],
        [],
        "UserRepository",
        []
    );

    
    $oKernel = new Kernel(
        $oConfig, 
        $oEnvironment, 
        [ // Middleware
            new ApiVersion('Example\Repository', $oConfig)
        ], 
        $oLoader);
    $oResponse = $oKernel->boot();
    $oResponse->send();

}